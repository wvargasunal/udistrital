<?php $con =new  mysqli("localhost", "root", "", "viajefeliz");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/css/bootstrap.min.css">
    
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/carousel.css">
    <link rel="stylesheet" href="./css/bar.css">
    <link rel="stylesheet" href="./css/css/style.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <title>VIAJEFELIZ.com</title>
</head>
<body>
    <header>
    <nav class="navbar navbar-expand-md navbar-ligth  fixed-top text-black" style="position: absolute; background-color:black;">
         <div id="logotipo">
             <img src="../img/logoo.svg" width="208" height="97"
             alt="Haz clic aquí para volver a la página de inicio">
         </div>
  
     <button class="navbar-toggle " type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarCollapse">
           
  
            <form class="form-inline mt-2 mt-md-0">
             <a href="../index.php" class="nav-link text-white"  >
                 VIAJE FELIZ SAS
                </a> 
  <!-------------------------------------------------------------Servicios----->
            <div class="dropdown">
               <a href="../#servicios" class="nav-link text-white"  style="cursor:pointer">
               Servicios
                </a>
             </div>
  <!------------------------------------------------------------trayectoria----->
             <div class="dropdown">
               <a href="trayectoria" class="nav-link text-white"  style="cursor:pointer">
                 Trayectoria
                </a>
               </div>
  
    <!------------------------------------CONTACTO ----------------------------------------------------->
              <div class="contacto">
               <a href="#contacto" class="nav-link text-white"  style="cursor:pointer">
                 <i class="fas fa-comments"></i>Contáctanos
                </a>
              </div>
            </form>
        </div>
     </nav>
    </header><hr>

<br>
<br>
<br>
<br>
   
 <div class="container">
 <form action="" class="form-group" method="POST">
              <div class="form-inline">
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Id alojamiento</span><hr>
                  <input type="text" name="ida" placeholder="Escribe tu destino ideal nosotros te llevamos" autofocus required class="form-control"><br>
                
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Tipo de alojamiento 1 casa 2 cabaña</span><hr>
                  <input type="text" name="ta" placeholder="Tipo" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Estado -1 disp -2 Reserv- 3 Alq</span><hr>
                  <input type="text" name="ea" placeholder="Estado" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Costo</span><br><hr>
                  <input type="text" name="costo" placeholder="Nª personas" autofocus required class="form-control">
                
                </div><br><br><br><br><br><br><br><hr>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">#habitaciones</span><hr>
                  <input type="text" name="ha" placeholder="Escribe tu destino ideal nosotros te llevamos" autofocus required class="form-control"><br>
                
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">#Baños</span><hr>
                  <input type="text" name="ba" placeholder="Tipo" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">#Capacid personas</span><hr>
                  <input type="text" name="person" placeholder="Estado" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Barrio</span><br><hr>
                  <input type="text" name="barr" placeholder="Nª personas" autofocus required class="form-control">
                
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Ubicacion</span><hr>
                  <input type="text" name="ubic" placeholder="Escribe tu destino ideal nosotros te llevamos" autofocus required class="form-control"><br>
                
                </div><br><br><br><br><br><br><br><hr>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Mascotas 1 Si, 2 No</span><hr>
                  <input type="text" name="mas" placeholder="Tipo" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Ventila 1 aire - ventil</span><hr>
                  <input type="text" name="vent" placeholder="Estado" autofocus required class="form-control">
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Ruta de la foto</span><br><hr>
                  <input type="text" name="foto" placeholder="Nª personas" autofocus required class="form-control">
                
                </div>
                <div class="col-sm-3 text-center">
                  <span class="btn btn-danger">Descripcion no mayor a 100 caracteres</span><br><hr>
                  <input type="text" name="des" placeholder="Nª personas" autofocus required class="form-control">
                
                </div>
                </div><br>
                
            </div>  
              <hr>
               <input type="submit" name="Buscar" value="Registro" class="btn btn-primary">
                <input type="reset" name="Cancelar" value="Cancelar" class="btn btn-info">
           
</form>         
                
              
       
</div>
    
</body>
</html>