<?php $con =new  mysqli("localhost", "root", "", "viajefeliz");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/css/bootstrap.min.css">
    
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/carousel.css">
    <link rel="stylesheet" href="./css/bar.css">
    <link rel="stylesheet" href="./css/css/style.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <title>VIAJEFELIZ.com</title>
</head>
<body>
    <header>
    <nav class="navbar navbar-expand-md navbar-ligth  fixed-top text-black" style="position: absolute; background-color:black;">
         <div id="logotipo">
             <img src="img/logoo.svg" width="208" height="97"
             alt="Haz clic aquí para volver a la página de inicio">
         </div>
  
     <button class="navbar-toggle " type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarCollapse">
           
  
            <form class="form-inline mt-2 mt-md-0">
             <a href="#actualidad" class="nav-link text-white"  >
                 VIAJE FELIZ SAS
                </a> 
  <!-------------------------------------------------------------Servicios----->
            <div class="dropdown">
               <a href="#servicios" class="nav-link text-white"  style="cursor:pointer">
               Servicios
                </a>
             </div>
  <!------------------------------------------------------------trayectoria----->
             <div class="dropdown">
               <a href="clientes.html" class="nav-link text-white"  style="cursor:pointer">
                 Trayectoria
                </a>
               </div>
  
    <!------------------------------------CONTACTO ----------------------------------------------------->
              <div class="contacto">
               <a href="#contacto" class="nav-link text-white"  style="cursor:pointer">
                 <i class="fas fa-comments"></i>Contáctanos
                </a>
              </div>
            </form>
        </div>
     </nav>
    </header><hr>

<br>
<br>
<br>
<br>
   
 <div class="container">
             <table class="table table-hover ">
            <h1>Para completar tu reserva vamos a pedirte tus datos</h1>
               <tbody>
                <tr>
                
                <?php   
                   
                    $ubi = $_GET['idu'];
                    $query="SELECT u.*, a.*, asig.* FROM  usuario u 
                    INNER JOIN asignaciones asig
                    ON u.id_usuario = asig.id_user
                    INNER JOIN alojamiento a
                    ON a.id_alojamiento = asig.id_aloj
                    WHERE U.id_usuario= '$ubi'" ;
                    $res=mysqli_query($con, $query);
                  
                     while($row=mysqli_fetch_array($res)){?>
                     <div class="text-center">
                     <div class="card card-hotel d-flex flex-column justify-content-between">
                       <p>
                       <?php echo '<h3 class="card-title" data-toggle="tooltip" data-placement="top" title="Los mejores platos, el mejor precio"> '.$row['ubicacion'].' </h3>'?>
                       <?php echo '<img src="'.$row['foto'].'" alt="Responsive image" class="img-thumbnail">'?>
                       <?php echo '<p> -Ubicacion '.$row['barrio'].' '.$row['descripcion'].'</p>'?>
                       <?php echo '<p style="visibility: collapse;" name="id">'.$row['id_alojamiento'].' </p>'?>
                       <?php if($row['estado_aloj'] ==1) echo '<p>-Estado del alojamiento Disponible</p>'?>
                       <?php echo '<h3>-Costo del alojamiento por noche:  '.$row['costo'].'</h3>'?>
                       <?php echo '<p>-Numero de personas por habitacion  '.$row['no_personas'].' </p>'?>
                       <?php if($row['tipo_aloj'] ==1) echo '<p>-Casa en la ciudad</p>'?>
                       <?php if($row['tipo_aloj'] ==2) echo '<p>-Cabaña en los alrededores de la ciudad</p>'?>
                       <?php if($row['mascotas'] ==1) echo '<p>-Se permiten mascotas</p>'?>
                       <?php if($row['mascotas'] ==2) echo '<p>-No se permiten mascotas</p>'?>
                       <?php $costo= $row['costo']?>
                       </p>
                       </div>
                     </div>
                  </tr>
                <?php } ?>
               </tbody>
            
              </table>
</div>
    
</body>
</html>