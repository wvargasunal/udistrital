<?php $con =new  mysqli("localhost", "root", "", "viajefeliz");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/css/bootstrap.min.css">
    
    <link href="fontawesome/css/all.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/carousel.css">
    <link rel="stylesheet" href="./css/bar.css">
    <link rel="stylesheet" href="./css/css/style.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <title>VIAJEFELIZ.com</title>
</head>
<body>
    <header>
    <nav class="navbar navbar-expand-md navbar-ligth  fixed-top text-black" style="position: absolute; background-color:black;">
         <div id="logotipo">
             <img src="img/logoo.svg" width="208" height="97"
             alt="Haz clic aquí para volver a la página de inicio">
         </div>
  
     <button class="navbar-toggle " type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarCollapse">
           
  
            <form class="form-inline mt-2 mt-md-0">
             <a href="#actualidad" class="nav-link text-white"  >
                 VIAJE FELIZ SAS
                </a> 
  <!-------------------------------------------------------------Servicios----->
            <div class="dropdown">
               <a href="#servicios" class="nav-link text-white"  style="cursor:pointer">
               Servicios
                </a>
             </div>
  <!------------------------------------------------------------trayectoria----->
             <div class="dropdown">
               <a href="clientes.html" class="nav-link text-white"  style="cursor:pointer">
                 Trayectoria
                </a>
               </div>
  
    <!------------------------------------CONTACTO ----------------------------------------------------->
              <div class="contacto">
               <a href="#contacto" class="nav-link text-white"  style="cursor:pointer">
                 <i class="fas fa-comments"></i>Contáctanos
                </a>
              </div>
            </form>
        </div>
     </nav>
    </header><hr>

<br>
<br>
<br>
<br>
   
 <div class="container">
             <table class="table table-hover ">
            <h1>Para completar tu reserva vamos a pedirte tus datos</h1>
               <tbody>
                <tr>
                
                <?php   
                   
                   if(isset($_GET['id'])){
                    $ubi = $_GET['id'];  
                    $query="SELECT * FROM alojamiento WHERE id_alojamiento='$ubi'" ;
                    $res=mysqli_query($con, $query);
                  }
                     while($row=mysqli_fetch_array($res)){?>
                     <div class="text-center">
                     <div class="card card-hotel d-flex flex-column justify-content-between">
                       <p>
                       <?php echo '<h3 class="card-title" data-toggle="tooltip" data-placement="top" title="Los mejores platos, el mejor precio"> '.$row['ubicacion'].' </h3>'?>
                       <?php echo '<img src="'.$row['foto'].'" alt="Responsive image" class="img-thumbnail">'?>
                       <?php echo '<p> -Ubicacion '.$row['barrio'].' '.$row['descripcion'].'</p>'?>
                       <?php echo '<p style="visibility: collapse;" name="id">'.$row['id_alojamiento'].' </p>'?>
                       <?php if($row['estado_aloj'] ==1) echo '<p>-Estado del alojamiento Disponible</p>'?>
                       <?php echo '<h3>-Costo del alojamiento por noche:  '.$row['costo'].'</h3>'?>
                       <?php echo '<p>-Numero de personas por habitacion  '.$row['no_personas'].' </p>'?>
                       <?php if($row['tipo_aloj'] ==1) echo '<p>-Casa en la ciudad</p>'?>
                       <?php if($row['tipo_aloj'] ==2) echo '<p>-Cabaña en los alrededores de la ciudad</p>'?>
                       <?php if($row['mascotas'] ==1) echo '<p>-Se permiten mascotas</p>'?>
                       <?php if($row['mascotas'] ==2) echo '<p>-No se permiten mascotas</p>'?>
                       <?php $costo= $row['costo']?>
                       </p>
                       </div>
                     </div>
                  </tr>
                <?php } ?>
               </tbody>
            
              </table>
            <div class="container" id="active">
            <h1>VIAJE FELIZ S.A.</h1><br>
                <h3>Para completar tu reserva vamos a pedirte tus datos</h3>
               <form action="" class="form" method="POST">
                   <div class="col-md-12">
                   <input type="text" name="nom" placeholder="Ingresa tu nombre compeleto" class="form-control">
                   </div><br>
                   <div class="col-md-12">
                   <input type="text" name="dir" placeholder="Ingresa tu direccion" class="form-control">
                   </div><br>
                   <div class="col-md-12">
                   <input type="text" name="tel" placeholder="Ingresa tu numero de celular" class="form-control">
                   </div><br>
                   <div class="col-md-12">
                   <input type="text" name="nac" placeholder="Ingresa tu nacionalidad" class="form-control">
                   </div><br>
                   <h4>Confirma las fechas de tu alojamiento</h4>
                   <div class="col-md-12">
                   <input type="date" name="ini" placeholder="Ingresa tu nacionalidad" class="form-control">
                   </div><br>
                   <div class="col-md-12">
                   <input type="date" name="fin" placeholder="Ingresa tu nacionalidad" class="form-control">
                   </div><br>                   
                   <hr><br>
                   <input type="submit" name="enviar" value="Siguiente" class="btn btn-primary" onclick=visible();>  
                  
               </form>
            </div>
            <?php 
               $con =new  mysqli("localhost", "root", "", "viajefeliz");
                      
              if(isset($_POST['nom'])){
                $nom = $_POST['nom'];
                $dir = $_POST['dir'];
                $tel = $_POST['tel'];
                $naci = $_POST['nac'];
                $cos = $row['costo'];
                $fecha1= new DateTime($_POST['ini']);
                $fecha2= new DateTime($_POST['fin']);
                $diff = $fecha1->diff($fecha2);
                $query= "INSERT INTO usuario(nombre, direccion, telefono, nacionalidad) VALUES('$nom', '$dir','$tel','$naci')";
                $res=mysqli_query($con, $query);
                mysqli_close($con);
               
            }    

           ?>
           
           
         <div class="row">
         <table class="table table-hover ">
               <tbody>
                <tr>
                <h1>Para completar tu reserva vamos a pedirte tus datos</h1><br>
                <?php   
                $con =new  mysqli("localhost", "root", "", "viajefeliz");
                   
                    if(isset($_POST['nom']))
                    {
                    $nom = $_POST['nom'];  
                    $costoal= $costo;
                    $query="SELECT * FROM usuario WHERE nombre='$nom'" ;
                    $res=mysqli_query($con, $query);
                   
                    }
                     while($rol=mysqli_fetch_array($res)){?>
                     <div class="col-md-12">
                     <div class="card card-hotel d-flex flex-column justify-content-between text-center">
                       <form action="pago.php" method="POST">
                       <?php echo '<br><p> '.$rol['nombre'].'</p>'?>
                       <?php echo '<br><p> '.$rol['direccion'].'</p>'?>
                       <?php echo '<br><p> '.$rol['telefono'].'</p>'?>
                       <?php echo '<br><p> '.$rol['nacionalidad'].'</p>'?>
                       <?php echo '<br><input type="text" name="iduser"  style="visibility: collapse;" autofocus required '.$rol['id_usuario'].'> </input>'?>
                       <?php echo '<h3>' .$diff->days * $costoal.' Costo total por '.$diff->days.' dias</h3>';?>
                       
                       <?php $iduser = $rol['id_usuario']?>
                       </form>
                      </div>
                      </div>
                       
                  </tr>
                <?php } ?>
               </tbody>
            
              </table>
         
         </div> 
         <div class="row">
         <table class="table table-hover ">
               <tbody>
                <tr>
                <h1>Para completar tu reserva vamos a pedirte tus datos</h1><br>
               <?php 
               $con =new  mysqli("localhost", "root", "", "viajefeliz");
                   
              if(isset($_GET['id'])){
                $ubi = $_GET['id'];
                $fecha1= $_POST['ini'];
                $fecha2= $_POST['fin'];
                $idu= $iduser;
                $query= "INSERT INTO asignaciones(pago, start_date, end_date, temporada, id_aloj, id_user) VALUES('0', '$fecha1','$fecha2',1, '$ubi', '$idu')";
                $res=mysqli_query($con, $query);
                mysqli_close($con);
                
            }else


           ?>
           <a href="costo.php?idu=<?php echo $rol['id_usuario']?>" class="form-control bg-danger text-center text-white"><span class="oi oi-plus" style="color: black;"></span>Contactar</a>
                        </tbody>
            
              </table>
         
         </div> 
                   
 </div>
 <script>
 function visible(){
    var div1 = document.getElementById("active");
    var align = div1.getAttribute("visibility");
    div1.getAttribute("collapse")

 }
 </script>
</body>
</html>